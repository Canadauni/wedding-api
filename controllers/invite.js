const mongoose = require('mongoose');
const Invite = require('../models/Invite');
const Attendee = require('../models/Attendee');

exports.getInvites = (req, res) => {
  Invite.find({}, (err, invite) => {
    if(err) {
      res.send(err);
    }
    res.json(invite);
  });
};

exports.postInvite = (req, res) => {
  let temp_invite = req.body.invite;
  let attendees = req.body.attendees;
  let id = new mongoose.Types.ObjectId();
  let new_invite = new Invite({
    _id: id,
    inviteName: temp_invite.inviteName
  });
  new_invite.save((err, invite) => {
    if(err) {
      return res.send(err);
    }
    attendees.forEach((i) => {
      let attendee = new Attendee({
        _id: new mongoose.Types.ObjectId(),
        name: i.name,
        attendeeInvite: new_invite._id
      });
      attendee.save((err) => {
        if(err) {
          return res.send(err);
        }
      });
    res.json(invite);
    });
  });
};

exports.searchInvite = (req, res) => {
  let name = '' + req.params.name;
  Attendee.
  findOne().
  where('name').equals(name).
  populate('attendeeInvite').
  exec((err, attendee) => {
    if(err) {
      return res.send(err);
    }
    res.json(attendee.attendeeInvite)
  });
};

exports.getOneInvite = (req, res) => {
  Invite.findById(req.params.inviteId, (err, invite) => {
    if(err) {
      throw new Error(err)
      return res.send(err);
    }
    res.json(invite);
  });
};

exports.updateInvite = (req, res) => {
  Invite.findByIdAndUpdate({ _id: req.params.inviteId}, req.body, {new: true}, (err, invite) => {
    if(err) {
      throw new Error(err)
      return res.send(err);
    }
    res.json(invite);
  });
};

exports.deleteInvite = (req, res) => {
  Invite.remove( {_id: req.params.inviteId}, (err, invite) => {
    if(err) {
      return res.send(err);
    }
    res.json( {message: 'invite deleted'} );
  });
};