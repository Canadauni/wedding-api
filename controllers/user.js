const mongoose = require('mongoose');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../models/User');

exports.registerUser = (req, res) => {
  if (!req.body.username || !req.body.password) {
    res.json({
      success: false,
      message: 'Please enter username and password.'
    });
  } else {
    let newUser = User({
      username: req.body.username,
      password: req.body.password
    });

    newUser.save(function(err) {
      if (err) {
        return res.json({
          success: false,
          message: 'That username already exits'
        });
      }
      res.json({
        success: true,
        message: 'Successfully created new user.'
      });
    });
  }
};

exports.logIn = (req, res) => {
  User.findOne({
    username: req.body.username
  }, function(err, user) {
    if (err) throw err;

    if (!user) {
      res.send({
        success: false,
        message: 'Authentication failed. User not found'
      });
    } else {
      user.comparePassword(req.body.password, function(err, isMatch) {
        if (isMatch && !err) {
          console.log(user);
          let token = jwt.sign(user.toJSON(), 'This is a secret key for auth by Christopher', {
            expiresIn: "2 days"
          });
          res.json({
            success: true,
            message: 'Authentication successful',
            token
          });
        } else {
          res.send({
            success: false,
            message: 'Auth failed.'
          })
        }
      })
    }
  })
}