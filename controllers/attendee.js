const mongoose = require('mongoose');
const Attendee = require('../models/Attendee');

exports.createAttendee = (req, res) => {
  let attendee = req.body.attendee;
  let id = new mongoose.Types.ObjectId();
  let newAttendee = new Attendee({
    _id: id,
    name: attendee.name
  });
  newAttendee.save((err, attendee) => {
    if (err) {
      return res.send(err);
    }
    res.json(attendee)
  });
};

exports.searchAttendee = (req, res) => {
  let name = req.params.name;
  Attendee.
  findOne().
  where('name').equals(name).
  exec((err, attendee) => {
    if(err) {
      return res.send(err);
    }
    res.json(attendee);
  });
};

exports.deleteAttendee = (req, res) => {
  Attendee.remove({_id: req.params.attendeeId}, (err, attendee) => {
    if(err) {
      return res.send(err);
    }
    res.json(attendee)
  })
};

exports.updateAttendee = (req, res) => {
  Attendee.findByIdAndUpdate({_id: req.params.attendeeId}, req.body, {new: true}, (err, attendee) => {
    if(err) {
      return res.send(err);
    }
    res.json(attendee);
  });
};

exports.searchAllAttendees = (req, res) => {
  Attendee.find({}, (err, attendees) => {
    if(err) {
      res.send(err);
    }
    res.json(attendees)
  });
};