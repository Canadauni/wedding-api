const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
const passport = require('passport');
const Rollbar = require('rollbar');

dotenv.load({ path: '.env' });
const rollbar = new Rollbar({
  accessToken: process.env.ROLLBAR_ID,
  captureUncaught: true,
  captureUnhandledRejections: true
});

app.use(cors());
app.use(rollbar.errorHandler());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI, {useMongoClient: true});
mongoose.connection.on('error', (err) => {
  console.error(err);
  console.log('MongoDB connection error.');
  process.exit();
});

app.use(passport.initialize());
require('./config/passport')(passport);

const routes = require('./routes/routes');
routes(app);

app.listen(process.env.PORT, () => {
  console.log('listening on port ' + process.env.PORT)
});