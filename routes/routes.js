module.exports = (app) => {
  const passport = require('passport')
  const attendeeRoute = require('../controllers/attendee');
  const userRoute = require('../controllers/user');

  app.route('/attendees')
    .get(passport.authenticate('jwt', {session: false}), attendeeRoute.searchAllAttendees)
    .post(passport.authenticate('jwt', {session: false}), attendeeRoute.createAttendee);

  app.route('/searchattendee/:name')
    .get(attendeeRoute.searchAttendee);

  app.route('/attendee/:attendeeId')
    .put(attendeeRoute.updateAttendee)
    .delete(passport.authenticate('jwt', {session: false}), attendeeRoute.deleteAttendee);

  app.route('/register')
    .post(userRoute.registerUser);

  app.route('/login')
    .post(userRoute.logIn);
}