const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const inviteSchema = new Schema({
  inviteName: {
    type: String,
    required: true
  },
  confirmed: {
    type: Boolean,
    default: false,
    required: true
  },
  attendees: [{
    type: Schema.Types.ObjectId,
    ref: 'Attendee'
  }]
});
const Invite = mongoose.model('Invite', inviteSchema);

module.exports = Invite;