const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const attendeeSchema = new Schema({
  _id: Schema.Types.ObjectId,
  name: String,
  email: String,
  visited: Boolean,
  confirmed: {
    type: Boolean,
    default: false,
    required: true
  }
});
const Attendee = mongoose.model('Attendee', attendeeSchema);

module.exports = Attendee;